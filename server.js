var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

const DEFAULT_PORT = 3000;
const UTF_8 = 'utf-8';

var MODELS_DIR = './models';

const ARRAY_ELEMENT = 'ARRAY_ELEMENT';
const OBJECT_ELEMENT = 'OBJECT_ELEMENT';

const CODE_CREATED = 201;
const BAD_REQUEST = 400;
const CODE_NOT_FOUND = 404;

var BODY_RESOURCE_NOT_FOUND = {
    success: false,
    message: 'Resource Not Found'
};
var BODY_SUCCESS = {
    success: true
};

var app = express();
app.use(bodyParser.json());

var isDefined = function (input) {
    return typeof input != 'undefined';
}

var UrlParser = {

    parse: function (url) {
        var children = url.split('/').filter(item => item != '');
        var root = children.shift();

        return {
            root: root,
            children: children,
            rootResourceIsDefined: function() {
                return isDefined(this.root);
            },
            hasMoreChildren: function () {
                return isDefined(this.children) && this.children.length > 0;
            },
            nextChild: function () {
                return this.children.shift();
            }
        };
    }
};

var ResourceManager = {
    storage: { // could be possible also to do all this operations async
        listItems: function() {
            return fs.readdirSync(MODELS_DIR);
        },
        itemExists: function(path) {
            return fs.existsSync(path);
        },
        readItem: function(path) {
            return JSON.parse(fs.readFileSync(path, UTF_8));
        },
        writeItem: function(path, content) {            
            fs.writeFileSync(path, JSON.stringify(content, null, 4));
        }
    },
    listRootResources: function() {
        return {
            found: true,
            resourceContent: this.storage.listItems()
                .map(item => item.substr(0, item.indexOf('.')))
        };        
    },
    loadResource: function (url) {
        if (!url.rootResourceIsDefined()) {
            return this.listRootResources();
        }

        var rootResourceName = url.root;
        console.log('loading resource with name:', rootResourceName);
        var path = MODELS_DIR + '/' + rootResourceName + '.json';

        if (!this.storage.itemExists(path)) {
            return {
                found: false,
                path: path
            };
        }

        var resourceRootContent = this.storage.readItem(path);

        var resourceContent = resourceRootContent;
        var resourceParent = null;
        var resourceKey = null;

        // might be a different combinations: [array|object] -> [object] -> [array|object] -> object -> [array|object] -> ...
        if (url.hasMoreChildren()) {

            do {
                var resourceId = url.nextChild();

                var curentLevelIsArray = Array.isArray(resourceContent);                
                if (curentLevelIsArray) {
                    resourceKey = {
                        value: resourceContent.findIndex(item => item.id == resourceId),
                        type: ARRAY_ELEMENT
                    };
                    resourceParent = resourceContent;
                    resourceContent = resourceContent.find(item => item.id == resourceId);
                } else {
                    resourceKey = {
                        value: resourceId,
                        type: OBJECT_ELEMENT
                    };
                    resourceParent = resourceContent;
                    resourceContent = resourceContent[resourceId];
                }
                if (!isDefined(resourceContent)) {
                    return {
                        path: path,
                        found: false,
                        failedOn: resourceId
                    };
                }

            } while (url.hasMoreChildren());

        } else {
            resourceContent = resourceRootContent;
        }

        var result = {
            found: true,
            path: path,
            resourceRootContent: resourceRootContent,
            resourceContent: resourceContent
        };

        if (resourceParent != null) {
            result.resourceParent = resourceParent;
        }

        if (resourceKey != null) {
            result.resourceKey = resourceKey;
        }

        return result;
    },
    appendResource: function (loadResourceResult, data) {
        var resourceRootContent = loadResourceResult.resourceRootContent;
        var resourceContent = loadResourceResult.resourceContent;

        if (Array.isArray(resourceContent)) { // append to array
            let itemsToAppend = Array.isArray(data) ? data : [data];

            for (itemToAppend of itemsToAppend) {
                if (!itemToAppend.hasOwnProperty('id')) {
                    if (resourceContent.length == 0) {
                        itemToAppend.id = 1;
                    } else {
                        var maxId = resourceContent.map(item => item.id).reduce((a, b) => Math.max(a, b));
                        itemToAppend.id = maxId + 1;
                    }
                }
                resourceContent.push(itemToAppend);
            }

        } else { // append to object
            Object.assign(resourceContent, data);
        }
        this.storage.writeItem(loadResourceResult.path, resourceRootContent);        
    },
    updateResource: function (loadResourceResult, data) {
        var resourceContent = loadResourceResult.resourceContent;
        if (Array.isArray(resourceContent)) {
            let itemsToUpdate = Array.isArray(data) ? data : [data];

            for (itemToUpdate of itemsToUpdate) {
                var item = resourceContent.find(item => item.id == itemToUpdate.id);
                if (item) {
                    Object.assign(item, itemToUpdate);
                    this.storage.writeItem(loadResourceResult.path, resourceContent);
                }
            }
        } else {
            Object.assign(resourceContent, data);
            this.storage.writeItem(loadResourceResult.path, loadResourceResult.resourceRootContent);
        }
    },
    deleteResource: function (loadResourceResult, data) {
        var resourceKey = loadResourceResult.resourceKey;

        if (loadResourceResult.resourceRootContent != loadResourceResult.resourceContent && resourceKey.type == ARRAY_ELEMENT) {
            loadResourceResult.resourceParent.splice(loadResourceResult.resourceKey.value, 1);
        } else {
            let resourceContent = loadResourceResult.resourceContent;
            if (Array.isArray(data) && Array.isArray(resourceContent)) { // batch delete
                for (itemId of data) {
                    let index = resourceContent.findIndex((item) => item.id == itemId);
                    if (index >= 0) {
                        resourceContent.splice(index, 1);
                    }
                }
            } else {
                delete loadResourceResult.resourceParent[loadResourceResult.resourceKey.value];
            }
        }
        this.storage.writeItem(loadResourceResult.path, loadResourceResult.resourceRootContent);
    }
};

app.all('*', (req, res) => {
    console.log(req.method, '>', req.url);
    res.type('json');

    var loadResourceResult = ResourceManager.loadResource(UrlParser.parse(req.url));
    if (!loadResourceResult.found) {
        res.statusCode = CODE_NOT_FOUND;
        res.send(BODY_RESOURCE_NOT_FOUND);
    }

    switch (req.method) {
        case 'GET':
            res.send(loadResourceResult.resourceContent);
            break;
        case 'POST':
            ResourceManager.appendResource(loadResourceResult, req.body);
            res.statusCode = CODE_CREATED;
            res.send(BODY_SUCCESS);
            break;
        case 'PUT':
            ResourceManager.updateResource(loadResourceResult, req.body);
            res.send(BODY_SUCCESS);
            break;
        case 'DELETE':
            ResourceManager.deleteResource(loadResourceResult, req.body)
            res.send(BODY_SUCCESS);
            break;
    }

});

app.listen(DEFAULT_PORT, () => console.log('Listening on port: ' + DEFAULT_PORT));