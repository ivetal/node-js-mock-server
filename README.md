# README #

## What is node-js-mock-server? ##

The project allows quickly to create mock REST API server that will respond with data that reflects all the sequence of preceding change requests (Create/Update/Delete).
No schemas for models or any other stuff related to db implementations is required, but files for root models must be created manually for now.
Data is stored in models folder as json files.
Also there is no need for coding/defining of API endpoints.
It would be enough for development/testing needs when creating applications that require communication by REST API without need to implement real server for the first time.

Key features:

* Supports any CRUD operations with nested resources of any depth (see samples of read requests).
* Batch operations support.
* Small size (just about 250 lines of code).

## How to start the server? ##

1. git clone ivetal@bitbucket.org:ivetal/node-js-mock-server.git
2. cd ./node-js-mock-server
3. npm install
4. npm start (or [nodemon server.js])

## Request methods mappings ##

 Http method  | Action 
------------- | -------
          GET | READ   
         POST | INSERT 
          PUT | UPDATE 
       DELETE | DELETE 


## Requests samples ##

### 1. Read ###

Read of resource of any depth could be done (the same for Create/Update/Delete).

For example:
~~~
GET http://localhost:3000/cars
GET http://localhost:3000/cars/3
GET http://localhost:3000/cars/3/options
GET http://localhost:3000/cars/3/options/
GET http://localhost:3000/cars/2/services/1
~~~
e.t.c.


### 2.1. Simple insert ###

~~~
POST http://localhost:3000/cars
{	
	"brand": "Skoda",
	"model": "Octavia",
	"options": {
		"climatControl": true,
		"rainSensor": true,
		"autoPilot": true,
		"shmorgalka": false
	}
}
~~~

~~~
Response code: 201 (Created)
Response body:
{
    "success": true
}
~~~

Optional id of item might be passed if needed.


### 2.2. Batch insert ###

~~~
POST http://localhost:3000/cars
[
	{
        "brand": "Chevrolet",
        "model": "Captiva"
    },
    {
        "brand": "Skoda",
        "model": "Roomster"
    }
]
~~~

~~~
Response code: 201 (Created)
Response body:
{
    "success": true
}
~~~

### 3.1. Simple update ###

~~~
PUT http://localhost:3000/cars
{
    "id": 3,
    "brand": "Chevrolet",
    "model": "Cruze",
    "options": {
        "autopilot": "yes"
    }
}
~~~

~~~
Response code: 200
Response body:
{
    "success": true
}
~~~

### 3.2. Batch update ###

~~~
PUT http://localhost:3000/cars
[
	{
        "brand": "Skoda",
        "model": "Roomster",
        "id": 5
    },
    {
        "brand": "Skoda",
        "model": "Yeti",
        "id": 6,
        "options": {
        	"Four-wheel drive": true
        }
    }
]
~~~

~~~
Response code: 200
Response body:
{
    "success": true
}
~~~


### 4.1. Simple delete ###

~~~
DELETE http://localhost:3000/cars/3
~~~


### 4.2. Batch delete ###

~~~
http://localhost:3000/cars
[2, 5, 7]
~~~


Сonstructive criticism and pull requests are welcome :)